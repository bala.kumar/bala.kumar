## Bala Kumar README
 
## About me
 
* I was born and raised in [**Chennai, India**](https://www.google.com/maps/d/embed?mid=1qJTcq5CaMdI4s4mNWp9Mi7QpJHQ).
* I live with my wife, daughter (kindergarten) and parents in **Chennai**.
* I enjoy **traveling**, **learning about new cultures**, **cooking**, **tamil music** and watch cricket.

## Work patterns
 
* I live in the [**Indian Standard Time**](https://www.timeanddate.com/time/zones/ist).
* I believe in and embrace [**non-linear workday routine**](https://about.gitlab.com/company/culture/all-remote/non-linear-workday/):
* I wake up around 7:00 AM, drop my wife and daughter, attend other chores and start my workday around **12.30 PM**.
* I prefer to attend team meetings that are scheduled in the EMEA region. And my workday typically has a good overlap with the EMEA timezone.
* I prefer to have sync ups and other meetings **during my evenings** (after 6:30 PM to 9.30 PM).
 
## Communicating with me
 
* I prefer [**asynchronous communication**](https://about.gitlab.com/handbook/values/#bias-towards-asynchronous-communication), it helps me prepare better answers and plan my workday.
* **Slack**:
  * You can send me a message on Slack at any time and I will respond during my working hours.
  * To be transparent with my team members I prefer keeping the conversation in our group channel ([#g_govern_threat_insights](https://gitlab.slack.com/archives/CV09DAXEW)).
  * I may send you a message on Slack outside your working hours, however, I do not expect you to respond immediately.
* **GitLab**:
  * To make sure I will see your message, please mention me (`@bala.kumar`) as it will automatically create a To-Do item for me.
